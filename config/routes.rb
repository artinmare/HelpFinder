Rails.application.routes.draw do
  get "application/index"

  resources :problems

  root "application#index"
end
